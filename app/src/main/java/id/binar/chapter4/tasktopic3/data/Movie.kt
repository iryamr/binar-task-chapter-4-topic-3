package id.binar.chapter4.tasktopic3.data

data class Movie(
    var id: String,
    var title: String,
    var score: String,
    var poster: Int,
)

package id.binar.chapter4.tasktopic3.util

import id.binar.chapter4.tasktopic3.R
import id.binar.chapter4.tasktopic3.data.Movie

object Data {

    fun generateMovies(): List<Movie> {
        val movies = ArrayList<Movie>()

        movies.add(
            Movie(
                id = "m1",
                title = "Alita: Battle Angel",
                score = "7.2",
                poster = R.drawable.poster_alita,
            )
        )

        movies.add(
            Movie(
                id = "m2",
                title = "Aquaman",
                score = "6.9",
                poster = R.drawable.poster_aquaman,
            )
        )

        movies.add(
            Movie(
                id = "m3",
                title = "How to Train Your Dragon",
                score = "7.8",
                poster = R.drawable.poster_how_to_train,
            )
        )

        movies.add(
            Movie(
                id = "m4",
                title = "Avengers: Infinity War",
                score = "8.3",
                poster = R.drawable.poster_infinity_war,
            )
        )

        movies.add(
            Movie(
                id = "m5",
                title = "Master Z: Ip Man Legacy",
                score = "6.2",
                poster = R.drawable.poster_master_z,
            )
        )

        movies.add(
            Movie(
                id = "m6",
                title = "Mortal Engines",
                score = "6.2",
                poster = R.drawable.poster_mortal_engines,
            )
        )

        movies.add(
            Movie(
                id = "m7",
                title = "Overlord",
                score = "6.7",
                poster = R.drawable.poster_overlord,
            )
        )

        movies.add(
            Movie(
                id = "m8",
                title = "Ralph Breaks the Internet",
                score = "7.2",
                poster = R.drawable.poster_ralph,
            )
        )

        movies.add(
            Movie(
                id = "m9",
                title = "Robin Hood",
                score = "5.9",
                poster = R.drawable.poster_robin_hood,
            )
        )

        movies.add(
            Movie(
                id = "m10",
                title = "Spider-Man: Into the Spider-Verse",
                score = "8.4",
                poster = R.drawable.poster_spiderman,
            )
        )

        return movies
    }
}
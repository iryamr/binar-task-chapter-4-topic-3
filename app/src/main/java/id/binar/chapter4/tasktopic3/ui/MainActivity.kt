package id.binar.chapter4.tasktopic3.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import id.binar.chapter4.tasktopic3.data.Movie
import id.binar.chapter4.tasktopic3.databinding.ActivityMainBinding
import id.binar.chapter4.tasktopic3.util.Data

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val adapter: MovieAdapter by lazy { MovieAdapter(::onMovieClicked) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setMovies()
    }

    private fun setMovies() {
        val movies = Data.generateMovies()

        adapter.submitList(movies)

        binding.rvMovies.adapter = adapter
        binding.rvMovies.layoutManager = GridLayoutManager(this@MainActivity, 2)
    }

    private fun onMovieClicked(movie: Movie) =
        Toast.makeText(this, movie.title, Toast.LENGTH_SHORT).show()
}